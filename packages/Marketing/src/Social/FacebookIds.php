<?php declare(strict_types=1);

namespace Pehapkari\Marketing\Social;

final class FacebookIds
{
    /**
     * @var string
     * The fastest way to detect: https://hellboundbloggers.com/find-facebook-profile-and-page-id/8516/
     * And verify: https://facebook.com/918297778220033
     */
    public const PEHAPKARI_PAGE_ID = '918297778220033';
}
